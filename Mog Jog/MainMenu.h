//
//  MainMenu.h
//  Mog Jog
//
//  Created by Ryan Salton on 04/02/2014.
//  Copyright (c) 2014 Ryan Salton. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "MainButton.h"

@interface MainMenu : SKScene <MainButtonDelegate>



@end
