//
//  MyScene.h
//  Mog Jog
//

//  Copyright (c) 2014 Ryan Salton. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "ColorButton.h"
#import "JumpLayer.h"
#import "MainButton.h"

@interface MyScene : SKScene <ButtonDelegate, JumpDelegate, SKPhysicsContactDelegate, MainButtonDelegate>

@property (nonatomic, strong) SKSpriteNode *player;

typedef NS_ENUM( NSUInteger, GateType )
{
    GateTypeRed,
    GateTypeBlue,
    GateTypeYellow,
    GateTypeWhite
};

@end
