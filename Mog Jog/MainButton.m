//
//  MainButton.m
//  Mog Jog
//
//  Created by Ryan Salton on 04/02/2014.
//  Copyright (c) 2014 Ryan Salton. All rights reserved.
//

#import "MainButton.h"

@implementation MainButton

-(instancetype)initWithImageNamed:(NSString *)name andText:(NSString *)text
{
    if (self = [super initWithImageNamed:@"mainButton"]) {
        self.userInteractionEnabled = YES;
        SKLabelNode *label = [SKLabelNode labelNodeWithFontNamed:@"ArialBlack"];
        label.text = text;
        label.color = [UIColor whiteColor];
        label.fontSize = 30;
        label.position = CGPointMake(label.position.x, label.position.y - 12);
        [self addChild:label];
    }
    return self;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.delegate mainButtonPressed];
}

@end
