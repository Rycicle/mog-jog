//
//  MyScene.m
//  Mog Jog
//
//  Created by Ryan Salton on 30/01/2014.
//  Copyright (c) 2014 Ryan Salton. All rights reserved.
//

#import "MyScene.h"
#import "MainButton.h"

#define ONE_GATE NO
#define MAKE_GATES YES
#define SOUNDON NO

UIDynamicAnimator *_animator;
UIGravityBehavior *_gravity;
UICollisionBehavior *_collision;

static const uint32_t playerCategory = 0x1 << 1;
static const uint32_t floorCategory = 0x1 << 2;
static const uint32_t gateCategoryRed = 0x1 << 3;
static const uint32_t gateCategoryBlue = 0x1 << 4;
static const uint32_t gateCategoryYellow = 0x1 << 5;
static const uint32_t gateCategoryWhite = 0x1 << 6;
static const uint32_t catastropheCategory = 0x1 << 7;
static const uint32_t youLoseCategory = 0x1 << 8;
static const uint32_t flapCategory = 0x1 << 8;

@import AVFoundation;

@implementation MyScene {
    int _currTime;
    SKSpriteNode *_floor;
    SKSpriteNode *youLose;
    SKSpriteNode *catastrophe;
    MainButton *retry;
    SKAction *_catAnimation;
    SKAction *_playerHitSound;
    NSMutableArray *_gates;
    NSMutableArray *_flaps;
    NSMutableArray *_newColor;
    NSMutableArray *_particles;
    NSArray *_gateColors;
    NSArray *_gateCategories;
    NSMutableArray *_buttons;
    NSMutableArray *_buttonsPressed;
    AVAudioPlayer *_backgroundMusicPlayer;
    BOOL jumping;
    BOOL dead;
    BOOL onegate;
}

-(id)initWithSize:(CGSize)size {    
    if (self = [super initWithSize:size]) {
        /* Setup your scene here */
        self.backgroundColor = [UIColor colorWithRed:155.0/255.0 green:0 blue:236.0/255.0 alpha:1];
        if(SOUNDON){
            [self playBackgroundMusic:@"FunkyMonkey.mp3"];
        }
        _playerHitSound =
        [SKAction playSoundFileNamed:@"Meow.mp3"
                   waitForCompletion:NO];
        
        SKSpriteNode *bg = [SKSpriteNode spriteNodeWithImageNamed:@"bg"];
        bg.position = CGPointZero;
        bg.anchorPoint = CGPointZero;
        bg.color = [UIColor colorWithRed:155.0/255.0 green:0 blue:236.0/255.0 alpha:1];
        bg.colorBlendFactor = 1.0;
        [self addChild:bg];
        
        self.physicsWorld.gravity = CGVectorMake(0, -2);
        self.physicsWorld.contactDelegate = self;
        
        _particles = [[NSMutableArray alloc] init];
        _gates = [[NSMutableArray alloc] init];
        _flaps = [[NSMutableArray alloc] init];
        _gateColors = [[NSArray alloc] initWithObjects:[UIColor redColor], [UIColor blueColor], [UIColor yellowColor], [UIColor whiteColor], nil];
        _buttons = [[NSMutableArray alloc] init];
        _buttonsPressed = [[NSMutableArray alloc] init];
        
//        UIView *playArea = [[UIView alloc] initWithFrame:self.frame];
//        [self.view addSubview:playArea];
        
        _floor = [SKSpriteNode spriteNodeWithColor:[UIColor blackColor] size:CGSizeMake(self.size.width, 20)];
//        _floor.anchorPoint = CGPointMake(0, 0);
        _floor.position = CGPointMake(self.size.width/2, 90);
        _floor.zPosition = 50;
        _floor.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:CGSizeMake(_floor.frame.size.width, _floor.frame.size.height)];
        _floor.physicsBody.dynamic = NO;
        _floor.physicsBody.categoryBitMask = floorCategory;
        _floor.physicsBody.collisionBitMask = playerCategory | floorCategory | gateCategoryRed | gateCategoryBlue | gateCategoryYellow | gateCategoryWhite;
        _floor.physicsBody.contactTestBitMask = playerCategory | floorCategory | gateCategoryRed | gateCategoryBlue | gateCategoryYellow | gateCategoryWhite;
        [self addChild:_floor];
        
        [self createPlayer];
        
//        _animator = [[UIDynamicAnimator alloc] initWithReferenceView:self.view];
////        _gravity = [[UIGravityBehavior alloc] initWithItems:@[self.player]];
//        _gravity.angle = 1;
//        _gravity.magnitude = 1;
//        [_animator addBehavior:_gravity];
        
//        [self makeButtonWithColor:[UIColor redColor] andX:30 andName:@"Red"];
//        [self makeButtonWithColor:[UIColor blueColor] andX:95 andName:@"Blue"];
//        [self makeButtonWithColor:[UIColor yellowColor] andX:160 andName:@"Yellow"];
//        [self makeButtonWithColor:[UIColor whiteColor] andX:225 andName:@"White"];
//        [self makeButtonWithColor:[UIColor whiteColor] andX:290 andName:@"White2"];
//        [self makeButtonWithColor:[UIColor yellowColor] andX:355 andName:@"Yellow2"];
//        [self makeButtonWithColor:[UIColor blueColor] andX:420 andName:@"Blue2"];
//        [self makeButtonWithColor:[UIColor redColor] andX:485 andName:@"Red2"];
        
        [self makeButtonWithColor:[UIColor redColor] andX:0 andName:@"Red"];
        [self makeButtonWithColor:[UIColor blueColor] andX:71 andName:@"Blue"];
        [self makeButtonWithColor:[UIColor yellowColor] andX:142 andName:@"Yellow"];
        [self makeButtonWithColor:[UIColor whiteColor] andX:213 andName:@"White"];
        [self makeButtonWithColor:[UIColor whiteColor] andX:284 andName:@"White2"];
        [self makeButtonWithColor:[UIColor yellowColor] andX:355 andName:@"Yellow2"];
        [self makeButtonWithColor:[UIColor blueColor] andX:426 andName:@"Blue2"];
        [self makeButtonWithColor:[UIColor redColor] andX:497 andName:@"Red2"];
        
        JumpLayer *jumpLayer = [[JumpLayer alloc] initWithColor:[UIColor whiteColor] size:self.size];
        jumpLayer.anchorPoint = CGPointZero;
        jumpLayer.delegate = self;
        [self addChild:jumpLayer];
        
        
    
        
    }
    return self;
}

-(void)createPlayer
{
    self.player = [SKSpriteNode spriteNodeWithImageNamed:@"Cat1"];
    [self.player setScale:0.6];
    self.player.color = [UIColor blackColor];
//    self.player.texture = [SKTexture textureWithImage:[UIImage imageNamed:@"PrintLeopard"]];
    self.player.colorBlendFactor = 1.0;
    //        self.player.anchorPoint = CGPointZero;
    self.player.position = CGPointMake(80, 200.0);
    self.player.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:self.player.size];
    self.player.physicsBody.dynamic = YES;
    self.player.physicsBody.categoryBitMask = playerCategory;
    self.player.physicsBody.collisionBitMask = playerCategory | floorCategory | gateCategoryRed | gateCategoryBlue | gateCategoryYellow | gateCategoryWhite;
    self.player.physicsBody.contactTestBitMask = playerCategory | floorCategory | gateCategoryRed | gateCategoryBlue | gateCategoryYellow | gateCategoryWhite;
    
    [self addChild:self.player];
    
    [self resetPlayerAnimation];
}

- (void)playBackgroundMusic:(NSString *)filename
{
    NSError *error;
    NSURL *backgroundMusicURL =
    [[NSBundle mainBundle] URLForResource:filename
                            withExtension:nil];
    _backgroundMusicPlayer =
    [[AVAudioPlayer alloc]
     initWithContentsOfURL:backgroundMusicURL error:&error];
    _backgroundMusicPlayer.numberOfLoops = -1;
    [_backgroundMusicPlayer prepareToPlay];
    [_backgroundMusicPlayer play];
}

- (void)makeButtonWithColor:(UIColor*)color andX:(float)x andName:(NSString *)name
{
    ColorButton *button = [[ColorButton alloc] initWithImageNamed:@"GradientButton" withColor:color andScale:0.49 andName:name];
    button.position = CGPointMake(x, 0);
    button.delegate = self;
    [_buttons addObject:button];
    [self addChild:button];
    
//    SKAction *actionButtonPress = [SKAction customActionWithDuration:0.2 actionBlock:^(SKNode *node, CGFloat elapsedTime) {
//        self.player.color = color;
//    }];
}

-(void)jumpPressed
{
    NSLog(@"JUMP PRESSED");
    if(!jumping){
        jumping = YES;
    NSMutableArray *textures =
    [NSMutableArray arrayWithCapacity:25];
    
    for (int i = 1; i < 26; i++) {
        NSString *textureName;
        if(i < 18 && i > 3){
            textureName = [NSString stringWithFormat:@"CatJump%d", 4];
        }
        else{
            textureName = [NSString stringWithFormat:@"CatJump%d", i];
        }
        SKTexture *texture =
        [SKTexture textureWithImageNamed:textureName];
        [textures addObject:texture];
    }
    _catAnimation =
    [SKAction animateWithTextures:textures timePerFrame:0.08];
    
    [_player runAction:[SKAction repeatActionForever:_catAnimation]];
    
    SKAction *delayJump = [SKAction waitForDuration:0.08 * 2];
    SKAction *actionJump = [SKAction customActionWithDuration:0.01 actionBlock:^(SKNode *node, CGFloat elapsedTime) {
        self.player.physicsBody.velocity = self.physicsBody.velocity;
        [self.player.physicsBody applyImpulse: CGVectorMake(0,45.0)];
    }];
    [self.player runAction:[SKAction sequence:@[delayJump, actionJump]]];
    
    SKAction *delayReset = [SKAction waitForDuration:0.08 * 18];
    SKAction *resetPlayer = [SKAction customActionWithDuration:0.01 actionBlock:^(SKNode *node, CGFloat elapsedTime) {
        [self resetPlayerAnimation];
        jumping = NO;
    }];
    [self runAction:[SKAction sequence:@[delayReset, resetPlayer]]];
    }
}

-(void)resetPlayerAnimation
{
    NSMutableArray *textures =
    [NSMutableArray arrayWithCapacity:12];
    
    for (int i = 1; i < 13; i++) {
        NSString *textureName =
        [NSString stringWithFormat:@"Cat%d", i];
        SKTexture *texture =
        [SKTexture textureWithImageNamed:textureName];
        [textures addObject:texture];
    }
    
    // 4
    _catAnimation =
    [SKAction animateWithTextures:textures timePerFrame:0.08];
    
    [_player runAction:[SKAction repeatActionForever:_catAnimation]];
}

-(void)buttonPressed:(UIColor *)color name:(NSString *)name
{
    NSLog(@"BUTTON PRESSED");
    [self checkPressedButtons];
}

-(void)buttonReleased:(NSString *)name
{
    [self checkPressedButtons];

}

-(void)checkPressedButtons {
//    [self playerExplode];
    for(int i = 0; i < [[self.player.physicsBody allContactedBodies] count]; i++){
        SKPhysicsBody *sprite = [self.player.physicsBody allContactedBodies][i];
        if(sprite.categoryBitMask != 4 && !dead){
            [self playerExplode];
            return;
        }
    }
    
    [_buttonsPressed removeAllObjects];
    UIColor *changeTo;
    
    for (int x = 0; x < _buttons.count; x++) {
        ColorButton *button = _buttons[x];
        if (button.isPressed) {
            [_buttonsPressed addObject:button];
        }
    }
    if (_buttonsPressed.count == 0) {
        changeTo = [UIColor blackColor];
        self.player.physicsBody.collisionBitMask = playerCategory | floorCategory | gateCategoryRed | gateCategoryBlue | gateCategoryYellow | gateCategoryWhite;
        self.player.physicsBody.contactTestBitMask = playerCategory | floorCategory | gateCategoryRed | gateCategoryBlue | gateCategoryYellow | gateCategoryWhite;
        SKAction *changeColor = [SKAction colorizeWithColor:changeTo colorBlendFactor:1.0 duration:0.2];
        [self.player runAction:changeColor];
        return;
    }
    ColorButton *button = _buttonsPressed[0];
    if (_buttonsPressed.count < 2) {
        if([button.name isEqualToString:@"Blue"] || [button.name isEqualToString:@"Blue2"]) {
            changeTo = [UIColor blueColor];
            self.player.physicsBody.collisionBitMask = playerCategory | floorCategory | gateCategoryRed | gateCategoryYellow | gateCategoryWhite;
            self.player.physicsBody.contactTestBitMask = playerCategory | floorCategory | gateCategoryRed | gateCategoryYellow | gateCategoryWhite;
        }
        else if ([button.name isEqualToString:@"Red"] || [button.name isEqualToString:@"Red2"]) {
            changeTo = [UIColor redColor];
            self.player.physicsBody.collisionBitMask = playerCategory | floorCategory | gateCategoryBlue | gateCategoryYellow | gateCategoryWhite;
            self.player.physicsBody.contactTestBitMask = playerCategory | floorCategory | gateCategoryBlue | gateCategoryYellow | gateCategoryWhite;
        }
        else if ([button.name isEqualToString:@"Yellow"] || [button.name isEqualToString:@"Yellow2"]) {
            changeTo = [UIColor yellowColor];
            self.player.physicsBody.collisionBitMask = playerCategory | floorCategory | gateCategoryRed | gateCategoryBlue | gateCategoryWhite;
            self.player.physicsBody.contactTestBitMask = playerCategory | floorCategory | gateCategoryRed | gateCategoryBlue | gateCategoryWhite;
        }
        else if ([button.name isEqualToString:@"White"] || [button.name isEqualToString:@"White2"]) {
            changeTo = [UIColor whiteColor];
            self.player.physicsBody.collisionBitMask = playerCategory | floorCategory | gateCategoryRed | gateCategoryBlue | gateCategoryYellow;
            self.player.physicsBody.contactTestBitMask = playerCategory | floorCategory | gateCategoryRed | gateCategoryBlue | gateCategoryYellow;
        }
    }
    else if (_buttonsPressed.count == 2){
        ColorButton *button2 = _buttonsPressed[1];
        if((([button.name isEqualToString:@"Blue"] || [button.name isEqualToString:@"Blue2"]) && ([button2.name isEqualToString:@"Red"] || [button2.name isEqualToString:@"Red2"])) || (([button.name isEqualToString:@"Red"] || [button.name isEqualToString:@"Red2"]) && ([button2.name isEqualToString:@"Blue"] || [button2.name isEqualToString:@"Blue2"]))) {
            changeTo = [UIColor colorWithRed:127.0/255.0 green:0.0/255.0 blue:194.0/255.0 alpha:1.0];
        }
        else if((([button.name isEqualToString:@"Yellow"] || [button.name isEqualToString:@"Yellow2"]) && ([button2.name isEqualToString:@"Red"] || [button2.name isEqualToString:@"Red2"])) || (([button.name isEqualToString:@"Red"] || [button.name isEqualToString:@"Red2"]) && ([button2.name isEqualToString:@"Yellow"] || [button2.name isEqualToString:@"Yellow2"]))) {
            changeTo = [UIColor colorWithRed:255.0/255.0 green:128.0/255.0 blue:0.0/255.0 alpha:1.0];
        }
        else if((([button.name isEqualToString:@"Blue"] || [button.name isEqualToString:@"Blue2"]) && ([button2.name isEqualToString:@"Yellow"] || [button2.name isEqualToString:@"Yellow2"])) || (([button.name isEqualToString:@"Yellow"] || [button.name isEqualToString:@"Yellow2"]) && ([button2.name isEqualToString:@"Blue"] || [button2.name isEqualToString:@"Blue2"]))) {
            changeTo = [UIColor colorWithRed:54.0/255.0 green:217.0/255.0 blue:0.0/255.0 alpha:1.0];
        }
        else if((([button.name isEqualToString:@"White"] || [button.name isEqualToString:@"White2"]) && ([button2.name isEqualToString:@"Red"] || [button2.name isEqualToString:@"Red2"])) || (([button.name isEqualToString:@"Red"] || [button.name isEqualToString:@"Red2"]) && ([button2.name isEqualToString:@"White"] || [button2.name isEqualToString:@"White2"]))) {
            changeTo = [UIColor colorWithRed:255.0/255.0 green:153.0/255.0 blue:255.0/255.0 alpha:1.0];
        }
        else if((([button.name isEqualToString:@"White"] || [button.name isEqualToString:@"White2"]) && ([button2.name isEqualToString:@"Blue"] || [button2.name isEqualToString:@"Blue2"])) || (([button.name isEqualToString:@"Blue"] || [button.name isEqualToString:@"Blue2"]) && ([button2.name isEqualToString:@"White"] || [button2.name isEqualToString:@"White2"]))) {
            changeTo = [UIColor colorWithRed:77.0/255.0 green:210.0/255.0 blue:255.0/255.0 alpha:1.0];
        }
        else if((([button.name isEqualToString:@"White"] || [button.name isEqualToString:@"White2"]) && ([button2.name isEqualToString:@"Yellow"] || [button2.name isEqualToString:@"Yellow2"])) || (([button.name isEqualToString:@"Yellow"] || [button.name isEqualToString:@"Yellow2"]) && ([button2.name isEqualToString:@"White"] || [button2.name isEqualToString:@"White2"]))) {
            changeTo = [UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:215.0/255.0 alpha:1.0];
        }
    }
    
    SKAction *changeColor = [SKAction colorizeWithColor:changeTo colorBlendFactor:1.0 duration:0.2];
    [self.player runAction:changeColor];
}

-(float)averageBetweenFloats:(float)floatA and:(float)floatB
{
    float floatNew = (floatA + floatB) / 2;
    if (floatNew < 0) {
        floatNew = floatNew * -1;
    }
    return floatNew;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    /* Called when a touch begins */
    
//    for (UITouch *touch in touches) {
//        CGPoint location = [touch locationInNode:self];
//        
//        SKSpriteNode *sprite = [SKSpriteNode spriteNodeWithImageNamed:@"Spaceship"];
//        
//        sprite.position = location;
//        
//        SKAction *action = [SKAction rotateByAngle:M_PI duration:1];
//        
//        [sprite runAction:[SKAction repeatActionForever:action]];
//        
//        [self addChild:sprite];
//    }
}

- (void)update:(CFTimeInterval)currentTime {
    /* Called before each frame is rendered */
    _currTime++;
    if (_currTime % 50 == 0) {
        [self renderBGWithItem:[self randomIntBetween:0 and:4]];
    }
    if (_currTime % 150 == 0) {
        //NSLog(@"CREATING GATE");
        if(!dead && MAKE_GATES && !onegate){
            if(ONE_GATE){
                onegate = YES;
            }
            [self createGate];
        }
    }
}

- (SKSpriteNode *)gateForType:(GateType)gateType
{
    SKSpriteNode *gate = [[SKSpriteNode alloc] init];
    gate.size = CGSizeMake(15, 180);
    gate.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:CGSizeMake(gate.frame.size.width, gate.frame.size.height)];
    gate.physicsBody.dynamic = NO;
    gate.anchorPoint = CGPointZero;
    if(onegate){
        gate.position = CGPointMake(self.size.width/2, _floor.position.y + _floor.size.height / 2);
    }
    else{
        gate.position = CGPointMake(self.size.width, _floor.position.y + _floor.size.height / 2);
    }
        [_gates addObject:gate];
    if (gateType == GateTypeRed)
    {
        gate.physicsBody.categoryBitMask = gateCategoryRed;
    }
    else if (gateType == GateTypeBlue)
    {
        gate.physicsBody.categoryBitMask = gateCategoryBlue;
    }
    else if (gateType == GateTypeYellow)
    {
        gate.physicsBody.categoryBitMask = gateCategoryYellow;
    }
    else if (gateType == GateTypeWhite)
    {
        gate.physicsBody.categoryBitMask = gateCategoryWhite;
    }
    gate.color = _gateColors[(int)gateType];
    
    return gate;
}

- (void)createGate
{
    SKSpriteNode *gate = [self gateForType:(GateType)[self randomIntBetween:0 and:(int)[_gateColors count]]];
    gate.name = @"gate";
    [self addChild:gate];
    
    // Create catflap
    SKSpriteNode *flap = [SKSpriteNode spriteNodeWithColor:[UIColor whiteColor] size:CGSizeMake(65.0, 3.0)];
    flap.position = CGPointMake(48, gate.position.y - 35);
    flap.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:flap.size];
    flap.physicsBody.dynamic = YES;
    flap.physicsBody.restitution = 0;
    flap.physicsBody.categoryBitMask = flapCategory;
//    flap.physicsBody.collisionBitMask = playerCategory | flapCategory | gateCategoryRed | gateCategoryBlue | gateCategoryYellow | gateCategoryWhite;
    flap.physicsBody.contactTestBitMask = playerCategory | flapCategory | gateCategoryRed | gateCategoryBlue | gateCategoryYellow | gateCategoryWhite;
    [self addChild:flap];
    [_flaps addObject:flap];
    
    SKPhysicsJointPin *joint = [SKPhysicsJointPin jointWithBodyA:flap.physicsBody bodyB:gate.physicsBody anchor:CGPointMake(flap.position.x - 30, flap.position.y)];
    [self.physicsWorld addJoint:joint];
    
    SKSpriteNode *flapStop = [SKSpriteNode spriteNodeWithColor:[UIColor clearColor] size:CGSizeMake(3.0, 3.0)];
    flapStop.position = CGPointMake(14, gate.position.y - 60);
    flapStop.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:flapStop.size];
    flapStop.physicsBody.dynamic = YES;
    flapStop.physicsBody.categoryBitMask = flapCategory;
    [self addChild:flapStop];
    [_flaps addObject:flapStop];
    
    SKPhysicsJointPin *stopJoint = [SKPhysicsJointPin jointWithBodyA:flapStop.physicsBody bodyB:gate.physicsBody anchor:flapStop.position];
    [self.physicsWorld addJoint:stopJoint];
    
    if(!onegate){
    SKAction *moveGate = [SKAction moveTo:CGPointMake(0-gate.size.width - flap.size.width, gate.position.y) duration:5.0];
    SKAction *removeGate = [SKAction customActionWithDuration:0.1 actionBlock:^(SKNode *node, CGFloat elapsedTime) {
        [_gates removeObject:gate];
        [_flaps removeObject:flap];
        [_flaps removeObject:flapStop];
        [gate runAction:[SKAction removeFromParent]];
        [flap runAction:[SKAction removeFromParent]];
        [flapStop runAction:[SKAction removeFromParent]];
    }];
    SKAction *actionGate = [SKAction sequence:@[moveGate, removeGate]];
    [gate runAction:actionGate];
    }
}

- (void)renderBGWithItem:(float)randomNumber
{
    SKSpriteNode *shape;
    //NSLog(@"%f", randomNumber);
    if(randomNumber == 0){
        shape = [SKSpriteNode spriteNodeWithImageNamed:@"Spiral"];
    }
    else if (randomNumber == 1) {
        shape = [SKSpriteNode spriteNodeWithImageNamed:@"Star"];
    }
    else {
        return;
    }
    
    shape.alpha = 0.25;
    shape.anchorPoint = CGPointZero;
    [shape setScale:[self randomFloatBetween:0.2 and:0.8]];
    shape.position = CGPointMake(self.size.width, [self randomIntBetween:0 and:self.size.height-shape.size.height]);
    [self addChild:shape];
    
    SKAction *moveBG = [SKAction moveTo:CGPointMake(0-shape.size.width, shape.position.y) duration:[self randomFloatBetween:2 and:5]];
    SKAction *rotateBG = [SKAction rotateByAngle:[self randomFloatBetween:4 and:8] duration:5];
    SKAction *removeBG = [SKAction removeFromParent];
    [shape runAction:[SKAction sequence:@[[SKAction group:@[moveBG, rotateBG]], removeBG]]];
}

- (float)randomFloatBetween:(float)smallNumber and:(float)bigNumber
{
    float diff = bigNumber - smallNumber;
    return (((float) (arc4random() % ((unsigned)RAND_MAX + 1)) / RAND_MAX) * diff) + smallNumber;
}

- (int)randomIntBetween:(int)smallNumber and:(int)bigNumber
{
    int lowerBound = smallNumber;
    int upperBound = bigNumber;
    int rndValue = lowerBound + arc4random() % (upperBound - lowerBound);
    return rndValue;
}

-(void)didBeginContact:(SKPhysicsContact *)contact
{
    NSLog(@"CONTACT MADE: %i", contact.bodyB.categoryBitMask);
//    SKPhysicsBody *firstBody, *secondBody;
    if([contact.bodyB.node.name isEqualToString:@"gate"]){
        [self playerExplode];
    }
    
//    if(contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask) {
//        firstBody = contact.bodyA;
//        secondBody = contact.bodyB;
//    }
//    else {
//        firstBody = contact.bodyB;
//        secondBody = contact.bodyA;
//    }
}

- (void)playerExplode
{
    
    if(!dead){
        dead = YES;
        [_backgroundMusicPlayer stop];
        [self runAction:_playerHitSound];
    int particleSize = 7;
    UIColor *particleColor = self.player.color;
    //cat tail
    for(int y = 0; y < 3; y++){
        SKSpriteNode *particle = [[SKSpriteNode alloc] initWithColor:particleColor size:CGSizeMake(particleSize, particleSize)];
        particle.position = CGPointMake(self.player.position.x - 45 + (y * particleSize), self.player.position.y + 3);
        particle.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:particle.size];
        particle.physicsBody.dynamic = YES;
        [_particles addObject:particle];
        [self addChild:particle];
    }
    // cat body
    for(int x = 0; x < 3; x++){
        for(int i = 0; i < 9; i++){
            SKSpriteNode *particle = [[SKSpriteNode alloc] initWithColor:particleColor size:CGSizeMake(particleSize, particleSize)];
            particle.position = CGPointMake(self.player.position.x - 20 + (i * particleSize), self.player.position.y + 8 - (x * particleSize));
            particle.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:particle.size];
            particle.physicsBody.dynamic = YES;
            [_particles addObject:particle];
            [self addChild:particle];
        }
    }
    // cat head
    for(int x = 0; x < 2; x++){
        for(int m = 0; m < 3; m++){
            SKSpriteNode *particle = [[SKSpriteNode alloc] initWithColor:particleColor size:CGSizeMake(particleSize, particleSize)];
            particle.position = CGPointMake(self.player.position.x + 40 + (m * particleSize), self.player.position.y + 15 - (x * particleSize));
            particle.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:particle.size];
            particle.physicsBody.dynamic = YES;
            [_particles addObject:particle];
            [self addChild:particle];
        }
    }
    // cat legs
    for(int y = 0; y < 3; y++){
        SKSpriteNode *particle = [[SKSpriteNode alloc] initWithColor:particleColor size:CGSizeMake(particleSize, particleSize)];
        particle.position = CGPointMake(self.player.position.x - 18, self.player.position.y - 14 - (y * particleSize));
        particle.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:particle.size];
        particle.physicsBody.dynamic = YES;
        [_particles addObject:particle];
        [self addChild:particle];
    }
    for(int y = 0; y < 3; y++){
        SKSpriteNode *particle = [[SKSpriteNode alloc] initWithColor:particleColor size:CGSizeMake(particleSize, particleSize)];
        particle.position = CGPointMake(self.player.position.x + 24, self.player.position.y - 14 - (y * particleSize));
        particle.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:particle.size];
        particle.physicsBody.dynamic = YES;
        [_particles addObject:particle];
        [self addChild:particle];
    }
    
        [self.player removeFromParent];
        if(!youLose){
        SKAction *delayGameOver = [SKAction waitForDuration:3.0];
        SKAction *dropYouLose = [SKAction customActionWithDuration:0.0 actionBlock:^(SKNode *node, CGFloat elapsedTime) {
            youLose = [SKSpriteNode spriteNodeWithImageNamed:@"youLose"];
            youLose.position = CGPointMake(self.size.width/2, self.size.height + youLose.size.height);
            youLose.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:CGSizeMake(youLose.size.width, youLose.size.height - 15)];
            youLose.physicsBody.dynamic = YES;
            youLose.zPosition = 50;
            youLose.physicsBody.categoryBitMask = youLoseCategory;
            youLose.physicsBody.collisionBitMask = youLoseCategory | floorCategory | catastropheCategory;
            youLose.physicsBody.contactTestBitMask = youLoseCategory | floorCategory | catastropheCategory;
            [self addChild:youLose];
        }];
        SKAction *delayAgain = [SKAction waitForDuration:0.5];
        SKAction *dropCatastrophe = [SKAction customActionWithDuration:0.0 actionBlock:^(SKNode *node, CGFloat elapsedTime) {
            catastrophe = [SKSpriteNode spriteNodeWithImageNamed:@"catastrophe"];
            catastrophe.position = CGPointMake(self.size.width/2, self.size.height + catastrophe.size.height);
            catastrophe.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:catastrophe.size];
            catastrophe.physicsBody.dynamic = YES;
            catastrophe.zPosition = 49;
            catastrophe.physicsBody.categoryBitMask = youLoseCategory;
            catastrophe.physicsBody.collisionBitMask = youLoseCategory | floorCategory | catastropheCategory;
            catastrophe.physicsBody.contactTestBitMask = youLoseCategory | floorCategory | catastropheCategory;
            [self addChild:catastrophe];
        }];
            SKAction *waitRetry = [SKAction waitForDuration:3.0];
        SKAction *addRetry = [SKAction customActionWithDuration:0.0 actionBlock:^(SKNode *node, CGFloat elapsedTime) {
            retry = [[MainButton alloc] initWithImageNamed:@"MainButton" andText:@"RETRY"];
            retry.position = CGPointMake(self.size.width/2, 40.0);
            retry.zPosition = 100;
            retry.delegate = self;
            [self addChild:retry];
        }];
            
        [self runAction:[SKAction sequence:@[delayGameOver, dropYouLose, delayAgain, dropCatastrophe, waitRetry, addRetry]]];
        }
        
    }
    
}

-(void)mainButtonPressed
{
    [youLose removeFromParent];
    youLose = nil;
    [catastrophe removeFromParent];
    dead = NO;
    for(int i = 0; i < _particles.count; i++){
        SKSpriteNode *particle = _particles[i];
        [particle removeFromParent];
    }
    [_particles removeAllObjects];
    for(int i = 0; i < _flaps.count; i++){
        SKSpriteNode *flap = _flaps[i];
        [flap removeFromParent];
    }
    [_flaps removeAllObjects];
    [retry removeFromParent];
    [self createPlayer];
    self.player.physicsBody.collisionBitMask = playerCategory | floorCategory | gateCategoryRed | gateCategoryBlue | gateCategoryYellow | gateCategoryWhite;
    self.player.physicsBody.contactTestBitMask = playerCategory | floorCategory | gateCategoryRed | gateCategoryBlue | gateCategoryYellow | gateCategoryWhite;
}

@end
