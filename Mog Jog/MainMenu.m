//
//  MainMenu.m
//  Mog Jog
//
//  Created by Ryan Salton on 04/02/2014.
//  Copyright (c) 2014 Ryan Salton. All rights reserved.
//

#import "MainMenu.h"
#import "MainButton.h"
#import "MyScene.h"

@implementation MainMenu

-(id)initWithSize:(CGSize)size {
    if (self = [super initWithSize:size]) {
        
        self.backgroundColor = [UIColor redColor];
        SKSpriteNode *bg = [SKSpriteNode spriteNodeWithImageNamed:@"bg"];
        bg.colorBlendFactor = 1.0;
        bg.color = [UIColor redColor];
        [self addChild:bg];
        
        SKSpriteNode *title = [SKSpriteNode spriteNodeWithImageNamed:@"title"];
        title.position = CGPointMake(self.size.width/2, self.size.height/2 + 40);
        [self addChild:title];
        
        MainButton *start = [[MainButton alloc] initWithImageNamed:@"start" andText:@"START"];
        start.position = CGPointMake(self.size.width/2, self.size.height/2 - 30);
        start.delegate = self;
        [self addChild:start];
        
    }
    return self;
}

-(void)mainButtonPressed
{
    MyScene *myScene = [[MyScene alloc] initWithSize:self.size];
    
    SKTransition *reveal =
    [SKTransition flipHorizontalWithDuration:1.0];
    [self.view presentScene:myScene transition: reveal];
}

@end
