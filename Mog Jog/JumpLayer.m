//
//  JumpLayer.m
//  Mog Jog
//
//  Created by Ryan Salton on 04/02/2014.
//  Copyright (c) 2014 Ryan Salton. All rights reserved.
//

#import "JumpLayer.h"

@implementation JumpLayer

-(instancetype)initWithColor:(UIColor *)color size:(CGSize)size
{
    if (self = [super initWithColor:color size:size]) {
        
        self.alpha = 0.0;
        self.userInteractionEnabled = YES;
        
    }
    return self;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.delegate jumpPressed];
}

@end
