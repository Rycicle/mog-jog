//
//  JumpLayer.h
//  Mog Jog
//
//  Created by Ryan Salton on 04/02/2014.
//  Copyright (c) 2014 Ryan Salton. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
@class JumpLayer;
@protocol JumpDelegate <NSObject>
-(void)jumpPressed;
@end

@interface JumpLayer : SKSpriteNode

@property (nonatomic, assign) id <JumpDelegate> delegate;

@end
