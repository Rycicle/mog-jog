//
//  ColorButton.m
//  Mog Jog
//
//  Created by Ryan Salton on 30/01/2014.
//  Copyright (c) 2014 Ryan Salton. All rights reserved.
//

#import "ColorButton.h"
#import "MyScene.h"

@implementation ColorButton

-(instancetype)initWithImageNamed:(NSString *)image withColor:(UIColor *)color andScale:(float)scale andName:(NSString *)name
{
    if (self = [super initWithImageNamed:image]) {
        self.anchorPoint = CGPointZero;
        self.zPosition = 100;
        self.scale = scale;
        self.color = color;
        self.colorBlendFactor = 0.8;
        self.name = name;
        self.userInteractionEnabled = YES;
        
        self.isPressed = NO;
    }
    return self;
}

//-(instancetype)initWithColor:(UIColor *)color size:(CGSize)size andName:(NSString *)name
//{
//    if (self = [super initWithColor:color size:size]) {
//        self.anchorPoint = CGPointZero;
//        self.zPosition = 100;
//        self.name = name;
//        self.userInteractionEnabled = YES;
//        
//        self.isPressed = NO;
//    }
//    return self;
//}


-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    self.isPressed = YES;
    [self.delegate buttonPressed:self.color name:self.name];
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    self.isPressed = NO;
    [self.delegate buttonReleased:self.name];
}

@end
