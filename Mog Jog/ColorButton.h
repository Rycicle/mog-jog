//
//  ColorButton.h
//  Mog Jog
//
//  Created by Ryan Salton on 30/01/2014.
//  Copyright (c) 2014 Ryan Salton. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
@class ColorButton;
@protocol ButtonDelegate <NSObject>
-(void)buttonPressed:(UIColor *)color name:(NSString *)name;
-(void)buttonReleased:(NSString *)name;
@end

@interface ColorButton : SKSpriteNode

@property (nonatomic, assign) id <ButtonDelegate> delegate;
@property (nonatomic, assign) BOOL isPressed;

-(instancetype)initWithImageNamed:(NSString *)image withColor:(UIColor *)color andScale:(float)scale andName:(NSString *)name;

@end
