//
//  MainButton.h
//  Mog Jog
//
//  Created by Ryan Salton on 04/02/2014.
//  Copyright (c) 2014 Ryan Salton. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@class MainButton;
@protocol MainButtonDelegate <NSObject>
-(void)mainButtonPressed;
@end

@interface MainButton : SKSpriteNode

@property (nonatomic, assign) id <MainButtonDelegate> delegate;
-(instancetype)initWithImageNamed:(NSString *)name andText:(NSString *)text;

@end
